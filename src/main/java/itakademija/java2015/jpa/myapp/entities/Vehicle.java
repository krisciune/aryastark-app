package itakademija.java2015.jpa.myapp.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Vehicle {

	@Id
	@GeneratedValue
	private Long id;
	private String name;
	
	//@ManyToOne (optional = true, fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "train_id")
	private Train train;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Train getTrain() {
		return train;
	}

	public void setTrain(Train train) {
		this.train = train;
	}

	@Override
	public String toString() {
		return ("vehicle id: " + id + " name: " + name);
	}
	
}
