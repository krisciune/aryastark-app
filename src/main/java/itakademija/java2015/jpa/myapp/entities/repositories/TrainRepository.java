package itakademija.java2015.jpa.myapp.entities.repositories;

import java.util.List;

import itakademija.java2015.jpa.myapp.entities.Train;

public interface TrainRepository {

	void save(Train newTrain);

	void delete(Train train);

	List<Train> findAll();

	Train findById(Long id);

}
