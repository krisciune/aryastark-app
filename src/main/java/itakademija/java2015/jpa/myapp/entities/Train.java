package itakademija.java2015.jpa.myapp.entities;

import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Train {

	@Id
	@GeneratedValue
	private Long id;

	private String number;

	@Temporal(TemporalType.DATE)
	private Date makeDate;

	private String maker;

	private String makeCity;

	@OneToMany(mappedBy = "train", orphanRemoval = true, fetch = FetchType.EAGER) //, cascade = { CascadeType.ALL }
	private List<Vehicle> vehicles;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + ((number == null) ? 0 : number.hashCode());
//		result = prime * result + ((maker == null) ? 0 : maker.hashCode());
//		result = prime * result + ((id == null) ? 0 : id.hashCode());
//		return result;
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj) {
//			return true;
//		}
//		if (obj == null) {
//			return false;
//		}
//		if (!(obj instanceof Train)) {
//			return false;
//		}
//		Train other = (Train) obj;
//		if (number == null) {
//			if (other.number != null) {
//				return false;
//			}
//		} else if (!number.equals(other.number)) {
//			return false;
//		}
//		if (id == null) {
//			if (other.id != null) {
//				return false;
//			}
//		} else if (!id.equals(other.id)) {
//			return false;
//		}
//		return true;
//	}

//	public void addVehicle(Vehicle vehicle) {
//
//		if (vehicles == null) {
//			vehicles = new ArrayList<>();
//		}
//
//		if (!vehicles.contains(vehicle)) {
//			vehicles.add(vehicle);
//		}
//
//	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public List<Vehicle> getVehicles() {
		return vehicles;
	}

	public void setVehicles(List<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}

	public String getMaker() {
		return maker;
	}

	public void setMaker(String maker) {
		this.maker = maker;
	}

	public String getMakeCity() {
		return makeCity;
	}

	public void setMakeCity(String makeCity) {
		this.makeCity = makeCity;
	}

	public Date getMakeDate() {
		return makeDate;
	}

	public void setMakeDate(Date makeDate) {
		this.makeDate = makeDate;
	}

}
