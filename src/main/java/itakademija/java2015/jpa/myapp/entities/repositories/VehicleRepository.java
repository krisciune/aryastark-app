package itakademija.java2015.jpa.myapp.entities.repositories;

import java.util.List;

import itakademija.java2015.jpa.myapp.entities.Train;
import itakademija.java2015.jpa.myapp.entities.Vehicle;

public interface VehicleRepository {

	public void save(Vehicle newVehicle);

	public void delete(Vehicle vehicle);

	public List<Vehicle> findAll();

	public List<Vehicle> findAllByTrain(Train currentTrain);

}
