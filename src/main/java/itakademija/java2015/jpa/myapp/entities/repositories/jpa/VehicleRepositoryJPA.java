package itakademija.java2015.jpa.myapp.entities.repositories.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import itakademija.java2015.jpa.myapp.entities.Train;
import itakademija.java2015.jpa.myapp.entities.Vehicle;
import itakademija.java2015.jpa.myapp.entities.Train_;
import itakademija.java2015.jpa.myapp.entities.Vehicle_;
import itakademija.java2015.jpa.myapp.entities.repositories.VehicleRepository;

public class VehicleRepositoryJPA implements VehicleRepository {

	static final Logger log = LoggerFactory.getLogger(TrainRepositoryJPA.class);

	private EntityManagerFactory entityManagerFactory;

	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.entityManagerFactory = emf;
	}
	
	private EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}
	
	@Override
	public void save(Vehicle vehicle) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			if (!entityManager.contains(vehicle) && vehicle.getId() != null) {
				vehicle = entityManager.merge(vehicle);
			}
			entityManager.persist(vehicle);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}

	}

	@Override
	public void delete(Vehicle vehicle) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			vehicle = entityManager.merge(vehicle);
			entityManager.remove(vehicle);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Vehicle> findAll() {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Vehicle> cq = cb.createQuery(Vehicle.class);
			Root<Vehicle> root = cq.from(Vehicle.class);
			cq.select(root);
			TypedQuery<Vehicle> q = entityManager.createQuery(cq);
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public List<Vehicle> findAllByTrain(Train train) {
		EntityManager entityManager = getEntityManager();
		try {
			train = entityManager.merge(train);
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Vehicle> cq = cb.createQuery(Vehicle.class);
			Root<Vehicle> root = cq.from(Vehicle.class);
			cq.select(root);
			cq.where(cb.equal(root.get(Vehicle_.train), train));
			TypedQuery<Vehicle> q = entityManager.createQuery(cq);
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

}
