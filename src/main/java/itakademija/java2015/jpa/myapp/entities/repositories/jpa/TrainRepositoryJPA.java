package itakademija.java2015.jpa.myapp.entities.repositories.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.omg.CORBA.TRANSACTION_MODE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import itakademija.java2015.jpa.myapp.entities.Train;
import itakademija.java2015.jpa.myapp.entities.Train_;
import itakademija.java2015.jpa.myapp.entities.repositories.TrainRepository;

public class TrainRepositoryJPA implements TrainRepository {

	static final Logger log = LoggerFactory.getLogger(TrainRepositoryJPA.class);

	private EntityManagerFactory entityManagerFactory;

	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.entityManagerFactory = emf;
	}
	
	private EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}
	
	@Override
	public void save(Train train) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			if (!entityManager.contains(train))
				train = entityManager.merge(train);
			entityManager.persist(train);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}

	}

	@Override
	public void delete(Train train) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			train = entityManager.merge(train);
			entityManager.remove(train);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}

	}

	@Override
	public List<Train> findAll() {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Train> cq = cb.createQuery(Train.class);
			Root<Train> root = cq.from(Train.class);
			cq.select(root);
			TypedQuery<Train> q = entityManager.createQuery(cq);
			return q.getResultList();
		} finally {
			entityManager.close();
		}
	}

	@Override
	public Train findById(Long id) {
		EntityManager entityManager = getEntityManager();
		try {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<Train> cq = cb.createQuery(Train.class);
			Root<Train> root = cq.from(Train.class);
			//cq.where(cb.equal(root.get(Author_.name), name));
			cq.where(cb.equal(root.get(Train_.id), id));
			TypedQuery<Train> q = entityManager.createQuery(cq);
			return q.getSingleResult();
		} finally {
			entityManager.close();
		}
	}


}
