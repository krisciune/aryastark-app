package itakademija.java2015.jpa.myapp.ui.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import itakademija.java2015.jpa.myapp.entities.Train;
import itakademija.java2015.jpa.myapp.entities.Vehicle;
import itakademija.java2015.jpa.myapp.entities.repositories.TrainRepository;

/**
 * This class is the Invoice list page controller. It deals with user commands
 * and navigation
 *
 */
public class TrainListPageBean implements Serializable { // this is the
															// Controller,
															// scope: request

	/**
	 * 
	 */
	private static final long serialVersionUID = -8143077418240345336L;

	static final Logger log = LoggerFactory.getLogger(TrainListPageBean.class);

	public static final String NAV_LIST_TRAINS = "list-trains";
	public static final String NAV_UPDATE_TRAIN = "update-train";
	public static final String NAV_UPDATE_VEHICLES = "update-vehicles";

	/**
	 * this class holds data in session
	 */
	public static class TrainListPageData implements Serializable { // this is
																	// the
																	// Model,
																	// scope:
																	// session

		/**
		 * 
		 */
		private static final long serialVersionUID = 5040310448257955354L;

		@Valid
		private Train currentTrain;

		public void init() {
			currentTrain = new Train();
		}

		public Train getCurrentTrain() {
			return currentTrain;
		}

		public void setCurrentTrain(Train currentTrain) {
			this.currentTrain = currentTrain;
		}

	}

	private TrainListPageData trainListPageData; // the model, session scope
	private TrainRepository trainRepo; // singleton

	public String create() {
		trainListPageData.setCurrentTrain(new Train());
		return NAV_LIST_TRAINS;
	}

	public String delete(Train train) {
		trainRepo.delete(train);
		return NAV_LIST_TRAINS;
	}

	public List<Train> getTrainList() {
		return trainRepo.findAll();
	}
	
	public List<Vehicle> findVehiclesByTrain() {
		return trainListPageData.getCurrentTrain().getVehicles();
	}
	
	public List<Vehicle> getFreshVehicleList() {
		return trainRepo.findById(trainListPageData.getCurrentTrain().getId()).getVehicles();
	}

	public String update(Train train) {
		trainListPageData.setCurrentTrain(train);
		return NAV_UPDATE_TRAIN;
	}

	public String save() {
		trainRepo.save(trainListPageData.getCurrentTrain());
		return NAV_LIST_TRAINS;
	}
	
	public String cancel() {
		trainListPageData.setCurrentTrain(new Train());
		return NAV_LIST_TRAINS;
	}

	public TrainRepository getTrainRepo() {
		return trainRepo;
	}

	public void setTrainRepo(TrainRepository trainRepo) {
		this.trainRepo = trainRepo;
	}

	public TrainListPageData getTrainListPageData() {
		return trainListPageData;
	}

	public void setTrainListPageData(TrainListPageData trainListPageData) {
		this.trainListPageData = trainListPageData;
	}

}
