package itakademija.java2015.jpa.myapp.ui.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import itakademija.java2015.jpa.myapp.entities.Train;
import itakademija.java2015.jpa.myapp.entities.Vehicle;
import itakademija.java2015.jpa.myapp.entities.repositories.TrainRepository;
import itakademija.java2015.jpa.myapp.entities.repositories.VehicleRepository;
import itakademija.java2015.jpa.myapp.ui.controllers.TrainListPageBean.TrainListPageData;

/**
 * This class is the Invoice list page controller. It deals with user commands and
 * navigation. Scope: request
 *
 */
public class VehicleListPageBean implements Serializable { // this is the Controller
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1389684115871062390L;

	static final Logger log = LoggerFactory.getLogger(VehicleListPageBean.class);
	
	public static final String NAV_LIST_TRAINS = "list-trains";
	public static final String NAV_UPDATE_TRAIN = "update-train";
	
	/**
	 * this class holds data in session and does nothing else whatsoever; scope: session
	 */
	public static class VehicleListPageData implements Serializable {  // this is the Model

		/**
		 * 
		 */
		private static final long serialVersionUID = -8131673305859876205L;

		@Valid
		private Vehicle currentVehicle;
		
		public void init() {
			currentVehicle = new Vehicle();
		}

		public Vehicle getCurrentVehicle() {
			return currentVehicle;
		}

		public void setCurrentVehicle(Vehicle currentVehicle) {
			this.currentVehicle = currentVehicle;
		}
	}

	private TrainRepository trainRepo;				// singleton
	private TrainListPageData trainListPageData;	// model: session scope
	private TrainListPageBean trainListPage;	// controller: request scope
	private VehicleRepository vehicleRepo;			// singleton
	private VehicleListPageData vehicleListPageData;// model: session scope


	public String addNewVehicle(Train currentTrain) {
		vehicleListPageData.setCurrentVehicle(new Vehicle());
		trainListPageData.setCurrentTrain(currentTrain);
		return TrainListPageBean.NAV_UPDATE_VEHICLES;
	}
	
	public String update(Vehicle vehicle) {
		vehicleListPageData.setCurrentVehicle(vehicle);
		return TrainListPageBean.NAV_UPDATE_VEHICLES;
	}
	
	public String save() {
		vehicleListPageData.getCurrentVehicle().setTrain(trainListPageData.getCurrentTrain());
		vehicleRepo.save(vehicleListPageData.getCurrentVehicle());
		vehicleListPageData.setCurrentVehicle(new Vehicle());
		return TrainListPageBean.NAV_UPDATE_VEHICLES;
	}
	
	public String delete(Vehicle vehicle) {
		vehicleRepo.delete(vehicle);
		return TrainListPageBean.NAV_UPDATE_VEHICLES;
	}
	
	public String cancel() {
		vehicleListPageData.setCurrentVehicle(new Vehicle());
		return TrainListPageBean.NAV_UPDATE_VEHICLES;
	}
	
	public List<Vehicle> findVehiclesByTrain() {
		return vehicleRepo.findAllByTrain(trainListPage.getTrainListPageData().getCurrentTrain());
	}
	
	public TrainRepository getTrainRepo() {
		return trainRepo;
	}
	public void setTrainRepo(TrainRepository trainRepo) {
		this.trainRepo = trainRepo;
	}
	public TrainListPageData getTrainListPageData() {
		return trainListPageData;
	}
	public void setTrainListPageData(TrainListPageData trainListPageData) {
		this.trainListPageData = trainListPageData;
	}
	public TrainListPageBean getTrainListPage() {
		return trainListPage;
	}
	public void setTrainListPage(TrainListPageBean trainListPageBean) {
		this.trainListPage = trainListPageBean;
	}
	public VehicleRepository getVehicleRepo() {
		return vehicleRepo;
	}
	public void setVehicleRepo(VehicleRepository vehicleRepo) {
		this.vehicleRepo = vehicleRepo;
	}
	public VehicleListPageData getVehicleListPageData() {
		return vehicleListPageData;
	}
	public void setVehicleListPageData(VehicleListPageData vehicleListPageData) {
		this.vehicleListPageData = vehicleListPageData;
	}

	
}
